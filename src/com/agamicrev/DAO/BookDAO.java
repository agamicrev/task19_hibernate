package com.agamicrev.DAO;

import com.agamicrev.model.BookEntity;

public interface BookDAO extends GeneralDAO<BookEntity, String> {

}
