package com.agamicrev.DAO;

import com.agamicrev.model.LibraryHasBookEntity;

public interface HasDAO extends GeneralDAO<LibraryHasBookEntity, LibraryHasBookEntity> {

}
