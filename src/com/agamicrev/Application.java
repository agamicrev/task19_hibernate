package com.agamicrev;

import com.agamicrev.ViewerAndController.Viewer;

public class Application {

  public static void main(String[] args) {
    Viewer viewer = new Viewer();
    viewer.show();
  }
}
