package com.agamicrev.ViewerAndController;

import java.sql.SQLException;

@FunctionalInterface
public interface Printable {

  void print() throws SQLException;
}
